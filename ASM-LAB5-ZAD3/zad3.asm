.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
atoi  PROTO :DWORD
lstrlenA PROTO :DWORD
GetTickCount PROTO 
nseed PROTO :DWORD 
nrandom PROTO :DWORD
.DATA
		cout		   dd ?
		cin			   dd ?
		tekst          db "Wprowadz liczbe: ",0
		rozmiart       db $ - tekst
		liczba         dd 0
		liczbaW		   db "Wprowadzona liczba to [ %i ]: %i ",10,0
		rliczbaW	   dd $ - liczbaW
		bufor          dw 128 DUP(?)
		bufor2          db 128 DUP(?)
		komunikat      db "Pierwsza liczba jest wieksza",0
		rkomunikat     dd $ - komunikat
		komunikat2     db "Podane liczby sa rowne",0
		rkomunikat2    dd $ - komunikat2
		komunikat3     db "Druga liczba jest wieksza",0
		rkomunikat3    dd $ - komunikat3
		zm             db 0
		zm2            db ?
		licznik		   dw 5d
		range		   dw ?
		wylosowana	   dw ?

		liczbaZ        dd 0
		rozmiar		   dd ?

		
.CODE
main proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	mov ECX, 10d
	pentela:

	push ECX
	mov range, 100d
	mov AX,range
	add AX, licznik
	mov range, ax

	invoke GetTickCount
	invoke nseed, EAX
	invoke nrandom, range
	mov wylosowana, AX
	pop ECX
	mov licznik, CX
	push ECX
	invoke wsprintfA, OFFSET bufor, OFFSET liczbaW, licznik, wylosowana
	invoke WriteConsoleA, cout, OFFSET bufor, rliczbaW,OFFSET liczba, 0
	pop ECX
	cmp ECX, 1d
	dec ECX
	jne pentela
invoke ExitProcess, 0

main endp

END